## Synopsis

Project Booilerplate using Webpack, Sass, Pug, Babel, Bootstrap and Jquery.

## Using with npm

Launch Development
```
npm run dev
```

Build for Production
```
npm run prod
```
